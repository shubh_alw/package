<?php
namespace Mangoit\Onepagecheckout\Controller\Account;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Controller\Account\ForgotPasswordPost;
use Magento\Framework\Controller\ResultFactory;

class ForgotPassword extends ForgotPasswordPost
{
    /** @var AccountManagementInterface */
    public $customerAccountManagement;

    /** @var Escaper */
    public $escaper;

    /**
     * @var Session
     */
    public $session;

    /**
     * Forgot customer password action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
            $email = (string)$this->getRequest()->getPost('email');
            $responseData = [];
            $responseData['error'] = false;

            if (!\Zend_Validate::is($email, 'EmailAddress')) {
                $this->session->setForgottenEmail($email);
                $responseData['message'] = __('Please correct the email address.');
            }

            try {
                $this->customerAccountManagement->initiatePasswordReset(
                    $email,
                    AccountManagement::EMAIL_RESET
                );
            } catch (NoSuchEntityException $e) {
                // Do nothing, we don't want anyone to use this action to determine which email accounts are registered.
                $responseData['message'] = __('We\'re unable to find the email.');
            } catch (\Exception $exception) {
                $responseData['message'] = __('We\'re unable to send the password reset email.');
            }
            $this->messageManager->addSuccessMessage($this->getSuccessMessage($email));
            $responseData['message'] = $this->getSuccessMessage($email);
        } else {
            $responseData['message'] = __('Please enter your email.');
        }
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($responseData);
        return $resultJson;
    }

    /**
     * Retrieve success message
     *
     * @param string $email
     * @return \Magento\Framework\Phrase
     */
    public function getSuccessMessage($email)
    {
        return __(
            'If there is an account associated with %1 you will receive an email with a link to reset your password.',
            $this->escaper->escapeHtml($email)
        );
    }
}
