<?php

namespace Mangoit\Onepagecheckout\Model\Payments\Paypal;

use \Magento\Paypal\Model\Config as paypalConfig;

class Config extends paypalConfig
{
    public function getBuildNotationCode()
    {
        return 'Mangoit_SI_MagentoCE_WPS';
    }
}
